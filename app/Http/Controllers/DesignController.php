<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class DesignController extends Controller
{
    //
    public function anyMethodNameThatYouWant(Request $request)
{
    $first_lable = $request->first_lable;
    $second_lable = $request->second_lable;
    $KEY_TWO=$request->KEY_TWO;
    // echo $number;
    // die();
    $this->updateDotEnv('VIEW_1', $first_lable);
    $this->updateDotEnv('VIEW_2',  $second_lable);
    $this->updateDotEnv('VALUE_TOW',  $KEY_TWO);
    // updateDotEnv('VIEW_ID', Auth::User()->id)
    // Config::set('admin.first_Lable', $first_lable);
    // Config::set('admin.second_Lable', $second_lable);
    return redirect()->route('home');
    // return view('welcome');
}

protected function updateDotEnv($key, $newValue, $delim='')
{

    $path = base_path('.env');
    // get old value from current env
    $oldValue = env($key);

    // was there any change?
    if ($oldValue === $newValue) {
        return;
    }

    // rewrite file content with changed data
    if (file_exists($path)) {
        // replace current value with new value 
        file_put_contents(
            $path, str_replace(
                $key.'="'.$oldValue.'"', 
                $key.'="'.$newValue.'"', 
                file_get_contents($path)
            )
        );
    }
}
}
